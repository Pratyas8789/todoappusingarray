# To-do app
Welcome to the To-Do App! This application is designed to help you keep track of your daily tasks and to-dos in a simple and organized way. 

# Features:

* Add tasks with a title name
* Mark tasks as complete
* Delete tasks

# Built With
* html
* css
* javascript

# Hosted link
* https://to-do-app-using-array-by-pratyas.netlify.app/

### *We hope you find this app helpful in organizing your tasks!*
