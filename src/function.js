const getAll = () => {
    container.innerHTML = "";
    deleteAllDiv.style.display = 'none';
    searchBar.style.display = 'block';
    buttonAll.style.borderBottom = "4px solid #2F80ED";
    buttonActive.style.borderBottom = "none";
    buttonCompleted.style.borderBottom = "none";
    tab = "all"

    toDo.forEach((toDoObj) => {
        if (toDoObj["checkedStatus"]) {
            container.innerHTML += `
                <div class="card">
                    <input onclick="checkStatus(${toDoObj["id"]})" class="checkbox lineThrough" type="checkbox" checked name="">             
                    <p class="lineThrough" >${toDoObj["text"]}</p>                    
                </div>`
        } else {
            container.innerHTML += `
                <div class="card">
                    <input onclick="checkStatus(${toDoObj["id"]})" class="checkbox " type="checkbox" name="">                
                    <p>${toDoObj["text"]}</p>                        
                </div>`
        }
    });
}

const getActive = () => {
    container.innerHTML = "";
    deleteAllDiv.style.display = 'none';
    searchBar.style.display = 'block';
    buttonAll.style.borderBottom = "none";
    buttonActive.style.borderBottom = "4px solid #2F80ED";
    buttonCompleted.style.borderBottom = "none";
    tab = "active"

    toDo.forEach((toDoObj) => {
        if (toDoObj["checkedStatus"] === false) {
            container.innerHTML += `
                <div class="card">
                    <input onclick="checkStatus(${toDoObj["id"]})" class="checkbox " type="checkbox" name="">                
                    <p>${toDoObj["text"]}</p>                    
                </div>`
        }
    });
}

const getCompleted = () => {
    container.innerHTML = "";
    deleteAllDiv.style.display = 'block';
    searchBar.style.display = 'none';
    buttonAll.style.borderBottom = "none";
    buttonActive.style.borderBottom = "none";
    buttonCompleted.style.borderBottom = "4px solid #2F80ED";
    tab = "completed";

    toDo.forEach((toDoObj) => {
        if (toDoObj["checkedStatus"]) {
            container.innerHTML += `
                <div class="card deleteicon">
                    <div class="flex">
                        <input onclick="checkStatus(${toDoObj["id"]})" class="checkbox" type="checkbox" checked name="">               
                        <p  class="lineThrough">${toDoObj["text"]}</p>                        
                    </div>
                    <div>
                        <i onclick="deleteDiv(${toDoObj["id"]})" class="fa fa-trash" aria-hidden="true"></i>
                    </div>
                </div>`
        }
    });
}

const addToDoTask = () => {
    const inputIdValue = inputTask.value;
    if (inputIdValue === "") {
        alert("Please enter detail");
    } else {
        let obj = {
            "id": uniqueId,
            "text": inputIdValue,
            "checkedStatus": false
        }
        toDo.push(obj);
        container.innerHTML = "";
        uniqueId++;
        inputTask.value = ""
        if (tab === "all") {
            getAll();
        } else if (tab === "active") {
            getActive();
        }
    }
}

const checkStatus = (Id) => {
    toDo.forEach((toDoObj) => {
        if (toDoObj["id"] === Id) {
            if (toDoObj["checkedStatus"]) {
                toDoObj["checkedStatus"] = false
            } else {
                toDoObj["checkedStatus"] = true
            }
        }
    });
    if (tab === "all") {
        getAll();
    } else if (tab === "active") {
        getActive();
    } else {
        getCompleted();
    }
}

const deleteAll = () => {
    let length=toDo.length-1
    for (let index = length; index >=0; index--) {        
        if (toDo[index]["checkedStatus"] === true) {
            toDo.splice(index, 1);
        }        
    }
    if (tab === "all") {
        getAll();
    } else if (tab === "active") {
        getActive();
    } else if (tab === "completed") {
        getCompleted();
    }
}

const deleteDiv = (Id) => {
    toDo.forEach((toDoObj) => {
        if (toDoObj["id"] === Id) {
            const index = toDo.indexOf(toDoObj)
            toDo.splice(index, 1);
        }
    });
    if (tab === "all") {
        getAll();
    } else if (tab === "active") {
        getActive();
    } else if (tab === "completed") {
        getCompleted();
    }
}