let uniqueId = 0;
const toDo = [];
const container = document.getElementById("container");
const searchBar = document.getElementById("searchBar");

const buttonAll = document.getElementById("buttonAll");
const buttonActive = document.getElementById("buttonActive");
const buttonCompleted = document.getElementById("buttonCompleted");
const deleteAllDiv=document.getElementById("deleteAllDiv");

let tab = "all";

let once = 0;
if (once === 0) {
    getAll();
    once++;
}

const inputTask = document.getElementById("inputId");
const completedElements = document.getElementById("completedElements");

inputTask.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
        addToDoTask();
    }
})